package ru.t1consulting.vmironova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.api.repository.model.ISessionRepository;
import ru.t1consulting.vmironova.tm.model.Session;

import javax.persistence.EntityManager;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}
